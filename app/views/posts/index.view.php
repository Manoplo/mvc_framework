<?php require_once APPROOT . '/views/partials/header.php'; ?>

<h1 style="font-family:sans-serif;"><?= isset($data['titulo']) ? $data['titulo'] : ""?></h1>
<hr/>

<h2 style="font-family:sans-serif;">Posts:</h2>

    <?php foreach ($data['posts'] as $post): ?>
        <h3 style="font-family:sans-serif; margin-top:1.5em;"><?= $post->title; ?></h3>
      
    <?php endforeach; ?>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>