<?php

class Post
{

    private $db;

    public function __construct()
    {
        // Al llamar al modelo POST, instancia una nueva conexión con la clase DB
        $this->db = new Database();
    }



    public function getPosts()
    {
        // Hago la query
        $this->db->query('SELECT * from posts');
        // Convierto los resultados del modelo Post a un array de objetos post
        $results = $this->db->getAllResults('Post');
        // Devuelvo el array. 
        return $results;
    }
}
