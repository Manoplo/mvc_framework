<?php

class Posts extends Controller
{

    private $postModel;

    public function __construct()
    {
        $this->postModel = $this->model('Post');
    }

    public function index()
    {
        $posts = $this->postModel->getPosts();

        $data = [
            'titulo' => 'Bienvenido al Blog',
            'posts' => $posts
        ];

        varAndDie($posts);

        return $this->view('posts/index', $data);
    }
}
