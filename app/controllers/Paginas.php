<?php

class Paginas extends Controller

{
    public function __construct()
    {

        /* echo 'Soy el controlador por defecto'; */
    }

    public function index()
    {

        $data = [
            'titulo' => 'Framework de Manuel Martín'
        ];

        return $this->view('paginas/index', $data);
    }

    public function about()
    {

        return $this->view('paginas/about');
    }

    public function actualizar($id)
    {
        echo "Metodo actualizar\n";
        echo $id;
    }
}
